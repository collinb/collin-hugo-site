---
title: "About Me"
date: 2020-03-22T22:31:13-07:00
draft: false
featured:
  url: me.jpg
weight: 1
summary: Who is Collin Barnwell? Information about me, my interests and my career overview.
---

Hi, I'm Collin Barnwell. When I was a kid, I wanted to be a nature photogropher or an inventor when I grew up.

Now I live in San Francisco, CA, USA, and am almost sort of an inventor. I like music and computer programming. I [read](/posts/books), run, climb, bike and camp. One time I tried to make a short film but it didn't turn out well. I speak English and Spanish.

## Career

### Verily Life Sciences

I'm currently a Software Engineer at Verily Life Sciences, where I primarily write firmware for futuristic medical devices. Right now, I'm working on enabling new functionality in a [novel hearing aid](https://www.massdevice.com/earlens-to-partner-with-verily-for-next-gen-earlens-hearing-device/). Prior to the hearing aid, I helped create an [implantable nerve stimulator](https://www.gsk.com/en-gb/media/press-releases/gsk-and-verily-to-establish-galvani-bioelectronics-a-new-company-dedicated-to-the-development-of-bioelectronic-medicines/). In a pinch, I sometimes work on Android apps and Chrome extensions, or anything else that I can help with and learn from.

### Google

Prior to Verily, I worked at Google enabling high performance networking for GPU ML-model training and other applications with low-level host libraries and NIC firmware. I did a two week rotation working at a data center diagnosing problems, replacing motherboards and swapping bad hard drives.

### Internships

I was lucky enough to intern at two early stage startups that have since gone on to be very successful companies.

At [Handshake](https://techcrunch.com/2018/10/31/handshake-a-linkedin-for-university-students-and-diversity-raises-40m-on-a-275m-valuation/undefined), before we'd signed any paying customers or raised any formal capital and were still working out of a house in Houghton, MI, because the rent was cheap, I worked with 3 other engineers adding features to a Rails application that eventually became the platform that today serves 5M+ students from all over the world. I also drove all the way around Lake Michigan twice that summer, meeting users at conferences and Universities.

At [G2Crowd](https://techcrunch.com/2018/10/11/g2-crowd-a-business-software-marketplace-raises-another-55m-at-just-under-a-500m-valuation/) I worked on a team of 5 web developers building the platform while it was still in Beta.

I also interned on the Stackdriver Cloud Monitoring team at Google.

## Education

I studied Computer Science at Northwestern University with a full-ride scholarship from the Evans Scholars Foundation. I explored a wide variety of new interests including poetry, russian literature, finance and [bizarre multimedia art](https://www.youtube.com/watch?v=4Ib05zMeYyQ&t=1474s), and played lots of ultimate frisbee.

For 5 months, I studied abroad in Lima, Peru, where I taught guitar and helped run youth leadership programs with an NGO called [Building Dignity](http://www.buildingdignity.org/).

## Music

I've been writing and recording songs in my bedroom since early high school. I play guitar, bass, harmonica, ukulele and sing and currently release indie folk music under the name [Axe Club](/music) on most digital platforms.
