---
title: "Favorite Books"
date: 2020-03-22T22:31:13-07:00
draft: false
---

In no particular order...

<!--more-->

## Fiction

Lost Empress by Sergio De La Pava

Special Topics in Calamity Physics by Marisha Pessl

Infinite Jest by David Foster Wallace

East of Eden by John Steinbeck

Anna Karenina by Leo Tolstoy

## Nonfiction

The Making of the Atomic Bomb by Richard Rhodes

Thinking, Fast and Slow by Daniel Kahneman

The Hidden Life of Trees: What They Feel, How They Communicate – Discoveries from a Secret World  by Peter Wohlleben

The Black Swan by Nassim Taleb

A People's History of the United States  by Howard Zinn

## Short Reads

The Curse of Bigness: Antitrust in the New Gilded Age by Tim Wu

[Untitled](_why-the-lucky-stiffs-printer-spool.pdf) by Why The Lucky Stiff
