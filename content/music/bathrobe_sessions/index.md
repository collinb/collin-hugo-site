---
title: "Axe Club - Bathrobe Sessions"
date: 2020-03-22T22:36:38-07:00
draft: false
featured:
  url: bathrobe_sessions.jpg
weight: 101
tags: ["Axe Club", "Music"]
---

My first album as Axe Club is a compilation of 14 songs written and recorded between 2014 and early 2019. It includes a wide variety of folk, indie rock and experimental music, with the beginnings of some jazz influence. Friends have compared some of the songs to Beirut, Bright Eyes and Tame Impala.

<!--more-->

If you like my music, or don't but you can spare the click, please follow me on Spotify!

{{< rawhtml >}}
<iframe src="https://open.spotify.com/follow/1/?uri=spotify:artist:1NxtWxfqFDBBAVaZrIxzMX?si=CYZBjPFPSomxHEfdT1r-cg&size=detail&theme=dark" width="300" height="56" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
{{< /rawhtml >}}

## Spotify
{{< rawhtml >}}
<iframe src="https://open.spotify.com/embed/album/4K9rryz34S2E5tADwpKj9Z" width="300" height="550" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
{{< /rawhtml >}}

## Bandcamp
{{< rawhtml >}}
<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=3077012797/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="http://axeclub.bandcamp.com/album/bathrobe-sessions">Bathrobe Sessions by Axe Club</a></iframe>
{{< /rawhtml >}}

## Other Platforms
{{< rawhtml >}}
<iframe width="100%" height="150" src="https://embed.song.link/?url=https%3A%2F%2Falbum.link%2Fus%2Fi%2F1462964929&theme=dark" frameborder="0" allowfullscreen sandbox="allow-same-origin allow-scripts allow-presentation allow-popups allow-popups-to-escape-sandbox"></iframe>
{{< /rawhtml >}}
