---
title: "Axe Club - Johnny Appleseed"
date: 2020-03-22T22:36:38-07:00
draft: false
featured:
  url: johnny_appleseed.jpg
tags: ["Axe Club", "Music"]
weight: 100
---

My first single of 2020 is written sarcastically from the perspective of a modern-era Johnny Appleseed who waves away all environmental problems in favor of convenience, and was inspired by my dislike for large parking lots.

<!--more-->

Search for it on your favorite music platform, or listen here with Spotify!

{{< rawhtml >}}
<iframe src="https://open.spotify.com/embed/track/1BtSfP4WB6V6r6aADU5ZH9" width="300" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
{{< /rawhtml >}}


If you like my music, or don't but you can spare the click, please follow me on Spotify!

{{< rawhtml >}}
<iframe src="https://open.spotify.com/follow/1/?uri=spotify:artist:1NxtWxfqFDBBAVaZrIxzMX?si=CYZBjPFPSomxHEfdT1r-cg&size=detail&theme=dark" width="300" height="56" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
{{< /rawhtml >}}